﻿#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <string> 
#include <typeinfo>
#include <fstream> 

#define RAND_MAX 255  

using namespace std; 
unsigned long int next1 = 1; 

unsigned char rand_gamma(void) { 
	next1 = next1 * 1103515245 + 12345; 
	return (unsigned char)(next1 / 65536) % (RAND_MAX + 1); 
} 

void srand(unsigned int seed) { 
	next1 = seed;
} 


char fun_cipher_gamma(char simbol, char gamma) { 
	return btowc(simbol) ^ btowc(gamma); 
}    

char fun_decipher_gamma(char simbol, char gamma) { 
	return btowc(simbol) ^ btowc(gamma); 
} 


int main() { 
	string cipher = "";
	string encrypted_word = "";
	string str = "", gamma = ""; 
	cout << "\nВведите слово\n";

	ifstream file1("D:\\coding\\laba_2\\cipher\\word.txt");
	getline(file1, str);
	cout << "\n" << str << "\n";

	srand(12022003); 
	
	for (int i = 0; i < str.length(); i++) { 
		gamma.push_back(rand_gamma()); 
	} 

	cout << "\n cipher with code: "; 
	for (int i = 0; i < str.length(); i++) {
		str[i] = fun_cipher_gamma(str[i], gamma[i]); 
		cout << str[i];
		cipher += str[i];
	} 
	system("pause");


	cout << "\n decode cipher: "; 
	for (int i = 0; i < str.length() && i < gamma.length(); i++) { 
		str[i] = fun_decipher_gamma(str[i], gamma[i]); 
		cout << str[i];
		encrypted_word += str[i];
	} 

	ofstream file2("D:\\coding\\laba_2\\cipher\\encrypted_word.txt");

	file2 << encrypted_word;
	
	cout << endl; 
	system("pause"); 
	return 0; 
} 