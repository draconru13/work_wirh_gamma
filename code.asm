.686
.MODEL TINY, C
STK SEGMENT STACK
EXTERN textResource : BYTE, textDestination : BYTE
STK ENDS
DATA SEGMENT BYTE PRIVATE 'DATA' USE32
const0 = 0
const1 = 1
constStrLen = 65536
text_OverFlow BYTE "Overflow the float digit!", 0Ah, 0Dh, 0
text_Gamma BYTE "Gamma = %d", 0Ah, 0Dh, 0
constT = 3
constV = 1
const8 = 8
constX = 15
const3 = 3
a DWORD 27
m DWORD ?
Gamma BYTE ?
Digit BYTE ?
DATA ENDS
CODE SEGMENT BYTE PRIVATE 'CODE'
ASSUME CS: CODE, DS: DATA, SS: STK
printf PROTO C arg1 : PTR BYTE, printlist : VARARG
PUBLIC C proc_ASM_Gamma_Simple
PUBLIC C proc_ASM_Gamma_Multi
PUBLIC C proc_a
PUBLIC C proc_m
proc_gamma PROTO C
ArrayR EQU textResource[BX]
ArrayD EQU textDestination[BX]
proc_ASM_Gamma_Simple PROC length_asm : BYTE
PUSH BP
XOR CX, CX
MOV ECX, DWORD PTR length_asm
CMP ECX, DWORD PTR constStrLen
JA @@ENDP_OverFlow
MOV EBX, DWORD PTR const0
@@Body_LOOP:
MOV AL, ArrayR
XOR AL, 1
MOV ArrayD, AL
INC BX
LOOP @@Body_LOOP
JMP @@ENDP
@@ENDP_OverFlow:
INVOKE printf, ADDR text_OverFlow
JMP @@ENDP
@@ENDP:
POP BP
RET
proc_ASM_Gamma_Simple EndP
proc_a PROC
PUSH BP
MOV EAX, DWORD PTR const8
MOV ESI, constT
IMUL ESI
MOV ESI, EAX
MOV EAX, const3
MOV EDI, constV
IMUL EDI
ADD EAX, ESI
MOV DWORD PTR a, EAX
POP BP
RET
proc_a EndP
proc_m PROC
PUSH BP
MOV EAX, DWORD PTR const1
SHL EAX, 6
MOV DWORD PTR m, EAX
POP BP
RET
proc_m EndP
proc_gamma PROC
PUSH BP
PUSH EAX
PUSH EDX
MOV EAX, DWORD PTR Gamma
MOV ESI, DWORD PTR a
IMUL ESI
IDIV m
MOV EAX, EDX
MOV DWORD PTR Gamma, EAX
XOR EDX, EDX
POP EDX
POP EAX
POP BP
RET
proc_gamma EndP
proc_ASM_Gamma_Multi PROC length_asm : BYTE
PUSH BP
XOR ECX, ECX
MOV ECX, DWORD PTR length_asm
CMP ECX, DWORD PTR constStrLen
JA @@ENDP_OverFlow
MOV EBX, WORD PTR const0
INVOKE proc_a
MOV DWORD PTR a, EAX
INVOKE proc_m
MOV DWORD PTR m, EAX
MOV EAX, DWORD PTR constX
MOV DWORD PTR Gamma, EAX
@@Body_LOOP:
MOV AL, ArrayR
INVOKE proc_gamma
.RADIX 16
XOR AL, Gamma
MOV ArrayD, AL
INC EBX
LOOP @@Body_LOOP
JMP @@ENDP
@@ENDP_OverFlow:
INVOKE printf, ADDR text_OverFlow
JMP @@ENDP
@@ENDP:
POP BP
RET
proc_ASM_Gamma_Multi EndP
CODE ENDS
END
